﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public int EndingScene;
    private int gameFinished;
    // Start is called before the first frame update
    void Start()
    {
        //tell next scene the game result
        //0 for didn't finish the game, 1 for finished
        PlayerPrefs.DeleteAll();
        gameFinished = 0;
        Solitaire.finishCount = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (Solitaire.finishCount == 4)
        {
            //You Finish!
            Debug.Log("You Win!");
            TimeManager.instance.SaveTimeStampToJson();
            gameFinished = 1;
            EndScene();
        }
    }
    public void EndScene()
    {
        PlayerPrefs.SetInt("result", gameFinished);
        SceneManager.LoadScene(EndingScene);
    }
}
