# Solitaire: FreeCells

This project is a solitaire game done by Unity engine. It follows the basic rule of [FreeCell](https://en.wikipedia.org/wiki/FreeCell).
There are 52 cards dealt into 8 cascades at the bottom. On the top there is 4 free cells and foundations. 
Players needs to move all the cards into the foundation pile.

This project is done within 24 hours.

## Basic Functionalities

This project follows basic FreeCell game rules and thus have following basic functionalities:

1. Cards are randomly dealt into the cascades
2. Players can drag and move a card into legal location such as open cells, empty cascades, or open foundations
3. Players can also drag and move a card or a tableau with another card or tableau to form a longer tableau
4. If all the cards is moved into foundation pile, player wins

## Additional features

The game also has some additional features:

1. There is a time counter in the game counts how long the player spends each time
2. Time is showed on the top of the game screen
3. The game also saves the time which player spend on current game into a JSON file, and record the fastest time that players spent on the game
4. The result with best record and current time spent will be showed when player finished the game

## Problems/Issues

1. If a tableau forms before the game begins, the system won't recognize that tableaus
2. Sometimes cards will be hidden by other cards
3. Selecting and draging cards sometimes is not accurate

## Future Improvement

If more time can be spent on this project, here are some changes I would love to have:


```
	1. Better Polished UI
		Right now the UI is too simple

	2. Undo Function
		People make mistakes, it will be nice to provide people a chance to regret

	3. Fix Tableau Issue
		As mentioned above, system won't recognize some tableaus which can be frustrating
	
	4. More Rules/Difficulties
		The game currently only follows the very basic rule of FreeCells, it will be more interesting if more rules can be implemented
```
